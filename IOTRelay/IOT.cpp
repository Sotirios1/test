#include "IOT.h"

IOTDevice::IOTDevice(String IOTID, unsigned int localUdpPort)
{
	this->ID = IOTID;
	this->localUdpPort = localUdpPort;
	this->maxSenderCountRegistered = 0;
	this->ccount = 0;
}


bool IOTDevice::setupIOT(String ssid, String pwd, bool isBeacon)
{
	connectWifi(ssid, pwd);

	String Message;
	String ipSender;
	unsigned int sportNo;

	int	count = 0;
	BroadCastMyPresence();
	do {

		if (isBeacon)
		if (count++ > 1000)
		{
			
			  BroadCastMyPresence();
			  count = 0;
			
		}

		if (gotAnyMessage(&Message, &ipSender, &sportNo))
		{
			if (isMessageReceivedForNewWifiConnection(Message))
			{

			}

			if (isMessageReceivedForNewSender(Message))
				break;
			
		}
		delay(10);
	} while (true);

	printCurrentStatus();

	return true;

}

void IOTDevice::disconnectWifi()
{
	Udp.stop();
	WiFi.disconnect();
}
bool IOTDevice::connectWifi(String ssid, String pwd)
{
	DEBUG_PRINT("Connecting to ");
	DEBUG_PRINTln(ssid);

	WiFi.begin(ssid.c_str(), pwd.c_str());

#ifdef DEBUG
	int count = 0;
#endif // DEBUG

	
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
#ifndef DEBUG
		DEBUG_PRINT(".");
#endif // !DEBUG
#ifdef DEBUG
		if (count++ > 10)
		{
			DEBUG_PRINT("Current status while making wifi connection: ");

			DEBUG_PRINT(WiFi.status());
			
		}
#endif // DEBUG
	}
	DEBUG_PRINTln("");
	DEBUG_PRINTln("WiFi connected");

	Udp.begin(localUdpPort);
	return true;
}

bool IOTDevice::tryConnectWifi(String ssid, String pwd, int noTrys)
{
	DEBUG_PRINT("Connecting to ");
	DEBUG_PRINTln(ssid);

	WiFi.begin(ssid.c_str(), pwd.c_str());
	int count = 0;
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		DEBUG_PRINT(".");

		if (count++ > noTrys)
		{
			DEBUG_PRINT("Current status while making wifi connection: ");

			DEBUG_PRINT(WiFi.status());

			return false;

		}

	}
	DEBUG_PRINTln("");
	DEBUG_PRINTln("WiFi connected");

	Udp.begin(localUdpPort);
	return true;
}

bool IOTDevice::bindToNewUDPPort(unsigned int portNumber)
{
	
	Udp.stop();
	delay(10);
	this->localUdpPort = portNumber;

	Udp.begin(localUdpPort);


	DEBUG_PRINT("Binded to new port. Listening on ");
	DEBUG_PRINT(WiFi.localIP());
	DEBUG_PRINTln(" Port=" + String(localUdpPort));


}

void IOTDevice::BroadCastMyPresence()
{
	String brParameter[3] = { IDi,IP,PORT };
	String values[3] = { ID, WiFi.localIP().toString(), String(localUdpPort) };

	String ip = WiFi.localIP().toString();
	String broadCastIP = ip.substring(0, ip.lastIndexOf('.')) + ".255";

	DEBUG_PRINTln("Broadcasting on : " + broadCastIP);

	
	IOTDevice::sendMessage(broadCastIP, localUdpPort, brParameter, values, 3);

}

bool IOTDevice::isMessageReceivedForNewWifiConnection(String Message, bool act)
{

	if (Message.indexOf(SSid) >= 0) // received change of wifi connection
	{
		disconnectWifi();
		if (act)
			if (tryConnectWifi(getValue(Message, SSid), getValue(Message, Pwd)))
			{

				printCurrentStatus();

			}
			else
				return false;

		return true;

	}

	return false;
}

bool IOTDevice::isMessageReceivedForNewSender(String Message, bool act)
{

	if (Message.indexOf(IP) >= 0 && Message.indexOf(PORT)>=0) //received IP address
	{
		if(act)
		  return AddSenderToService(Message);
	}

	return false;

}

bool IOTDevice::isIOTConfigureChangeRequest(String Message, bool act)
{

	if (isMessageReceivedForNewWifiConnection(Message, act) || isMessageReceivedForNewSender(Message, act))
		return true;
	else
		return false;

}

bool IOTDevice::AddSenderToService(String controlMessage)
{
	String sender = getValue(controlMessage, IP);
	unsigned int senderPort = getValue(controlMessage, PORT).toInt();

	return AddSenderToService(sender, senderPort);

}
bool  IOTDevice::AddSenderToService(String sender, unsigned int senderPort)
{
	if (sender == WiFi.localIP().toString())
		return false;

	String brParameter[4] = { IDi,IP,PORT, "Status" };
	for (int i = 0; i < maxSenderCountRegistered; i++)
	{
		if (sender == this->senderIP[i] && senderPort == this->senderUdpPort[i])
		{
			
			String values[4] = { ID, WiFi.localIP().toString(), String(localUdpPort), "AddedAlready" };
			sendMessage(sender, senderPort, brParameter, values, 4);

			return false;
		}
	}

	if (maxSenderCountRegistered >= MAX_NO_SENDER)
	{
		DEBUG_PRINT("Can not add more senders ...");
		
		String values[4] = { ID, WiFi.localIP().toString(), String(localUdpPort), "Failed" };
		sendMessage(sender, senderPort, brParameter, values, 4);

		return false;
	}

	this->senderIP[this->maxSenderCountRegistered] = sender;
	this->senderUdpPort[this->maxSenderCountRegistered] = senderPort;

	String values[4] = { ID, WiFi.localIP().toString(), String(localUdpPort), "Added" };
	sendMessage(sender, senderPort, brParameter, values, 4);
	this->maxSenderCountRegistered++;


	printCurrentStatus();


	return true;

}
bool IOTDevice::startConfiguring(String ssid, String pwd)
{

	connectWifi(ssid, pwd);
	Udp.begin(localUdpPort);

	DEBUG_PRINT("Broadcasting... Listening on ");
	DEBUG_PRINT(WiFi.localIP());
	DEBUG_PRINTln(" Port=" + String(localUdpPort));


	String brParameter[3] = { IDi,IP,PORT };
	String values[3] = { ID, WiFi.localIP().toString(), String(localUdpPort) };
	String messageID = createMessage(brParameter, values, 3, Delimiter);
	do {

		int packetSize = Udp.parsePacket();
		DEBUG_PRINT(".");
		if (packetSize)
		{

			// receive incoming UDP packets
#ifdef DEBUG
			OUTPUTSteps.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
#endif // DEBUG

			
			char incomingPacket[255];  // buffer for incoming packets
			int len = Udp.read(incomingPacket, 255);
			if (len > 0)
			{
				incomingPacket[len] = 0;
			}
#ifdef DEBUG
			OUTPUTSteps.printf("UDP packet contents: %s\n", incomingPacket);
#endif // DEBUG

			String messageRecvd = String(incomingPacket);

			if (messageRecvd.indexOf(SSid) >= 0)
			{

				lockSendersDetails();
				DEBUG_PRINTln("Received SSID to connect to...");
				String parameters[2];
				String vals[2];

				parseString(messageRecvd, 2, parameters, vals, Delimiter);

				Udp.stop();
				WiFi.disconnect();

				DEBUG_PRINTln("Connecting to " + vals[0] + " " + vals[1]);
				connectWifi(vals[0], vals[1]);
				Udp.begin(localUdpPort);

				String newvalue[3] = { ID, WiFi.localIP().toString(), String(localUdpPort) };
				sendMessage(brParameter, newvalue, 3);
				DEBUG_PRINTln("Waiting to serve a device....");
				do {
					DEBUG_PRINT(".");
					String Message;
					String ipSender;
					unsigned int sportNo;
					if (gotAnyMessage(&Message, &ipSender, &sportNo))
					{
						int index = Message.indexOf(CODE);
						if (index >= 0)
						{
							String parameters[1];
							String vals[1];

							parseString(Message.substring(index), 1, parameters, vals, Delimiter);

							if (ID.indexOf(vals[0]) >= 0)
							{

								String newvalues[3] = { ID, WiFi.localIP().toString(), String(localUdpPort) };
								sendMessage(brParameter, newvalues, 3);

								//DEBUG_PRINTln("Serving to " + senderIP.toString() + ":" + String(senderUdpPort));

								lockSendersDetails();
								break;
							}
						}
					}

					delay(1000);
				} while (true);


				break;
			}
			else
			{
				Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
				Udp.write(messageID.c_str());
				Udp.endPacket();

			}

		}
		delay(1000);
	} while (true);

	DEBUG_PRINTln("IOT configured and ready...");
	return true;
}

String IOTDevice::getValue(String message, String key)
{
	int index = message.indexOf(key) + key.length() + 1;
	int indexE = message.indexOf(Delimiter, index);

	String val = message.substring(index, indexE);
	DEBUG_PRINTln(val);
	return val;
}

String IOTDevice::createMessage(String params[], String values[], int n, char delimiter)
{
	String res = "";
	for (int i = 0; i < n - 1; i++)
		res += params[i] + ":" + values[i] + delimiter;

	res += params[n - 1] + ":" + values[n - 1];
	return res;
}

void split(String data, char delimiter, int n, String output[])
{

	int c = 0;
	for (int i = 0; i < data.length() && c < n; i++)
	{
		char ch = data.charAt(i);
		if (ch == delimiter)
			c++;
		else
			output[c] += data.charAt(i);

	}

}
void IOTDevice::parseString(String data, int n, String parameter[], String values[], char delimiter)
{
	String di[n];
	split(data, delimiter, n, di);


	for (int i = 0; i < n; i++)
	{
		String dv[2];
		split(di[i], ':', 2, dv);
		parameter[i] = dv[0];
		values[i] = dv[1];
	}
}
void IOTDevice::lockSendersDetails()
{
	AddSenderToService(Udp.remoteIP().toString(), Udp.remotePort());
}
void IOTDevice::sendMessage(String sender, unsigned int sender_portNo, String params[], String values[], int n)
{
	String Message = createMessage(params, values, n, Delimiter);
	String bytesOP[4];
	split(sender, '.', 4, bytesOP);
	IPAddress ip(bytesOP[0].toInt(), bytesOP[1].toInt(), bytesOP[2].toInt(), bytesOP[3].toInt());
	Udp.beginPacket(ip, sender_portNo);
	Udp.write(Message.c_str());
	Udp.endPacket();
	

}



void IOTDevice::sendMessage(String params[], String values[], int n)
{

	for(int i =0; i < this->maxSenderCountRegistered; i++)
	sendMessage(senderIP[i], senderUdpPort[i], params, values, n);

}

bool IOTDevice::sendMessageOnlyIfDifferent(String params[], String values[], int n, int thresholdSendAnyways)
{
	bool different = false;

	for (int i = 0; i < n; i++)
		if (values[i] != oldvalues[i])
		{
			different = true; break;
		}

	if (different || ccount > thresholdSendAnyways)
	{
		for (int i = 0; i < n; i++)
			oldvalues[i] = values[i];
		sendMessage(params, values, n);
		ccount = 0;

		return true;
	}
	else
	{
		ccount++;
		return false;
	}



}
bool IOTDevice::gotAnyMessage(String *message, String *ipSender, unsigned int *senderUdpPort)
{
	int packetSize = Udp.parsePacket();
	//DEBUG_PRINT(".");
	if (packetSize)
	{
		char incomingPacket[255];  // buffer for incoming packets
		int len = Udp.read(incomingPacket, 255);
		if (len > 0)
		{
			incomingPacket[len] = 0;
		}
#ifdef DEBUG
		OUTPUTSteps.printf("UDP packet contents: %s\n", incomingPacket);
#endif // DEBUG

		

		*message = String(incomingPacket);
		*ipSender = Udp.remoteIP().toString();
		*senderUdpPort = Udp.remotePort();

		latestSenderIP = Udp.remoteIP().toString();
		latestSenderPort = Udp.remotePort();

		return true;

	}

	return false;
}

String IOTDevice::getNewMessage(bool processConfigurationIOTAlso)
{
	String Message = "";
	String ipSender;
	unsigned int sportNo;

		if (gotAnyMessage(&Message, &ipSender, &sportNo))
		{
			if (processConfigurationIOTAlso)
				isIOTConfigureChangeRequest(Message);
		}
		
		return Message;
}

String  IOTDevice::getLatestSenderIP(unsigned int *senderUdpPort)
{
	*senderUdpPort = latestSenderPort;

	return latestSenderIP;

}
void IOTDevice::printCurrentStatus()
{

	OUTPUTSteps.print("Broadcasting... Listening on ");
	OUTPUTSteps.print(WiFi.localIP());
	OUTPUTSteps.println(" Port=" + String(localUdpPort));

	if(this->maxSenderCountRegistered == 0)
		OUTPUTSteps.println("Waiting to serve a device....");
	else
	{
		OUTPUTSteps.println("Senders List: ");
		for (int i = 0; i < maxSenderCountRegistered; i++)
			OUTPUTSteps.println(String(i) + " " + senderIP[i] + ":" + String(senderUdpPort[i]));
		OUTPUTSteps.println("IOT configured and ready...");
	}

}
