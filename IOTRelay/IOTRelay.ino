#include "IOT.h"
#define SSID "Universee"
#define PWD "universee"
#define IDNo "1111" //<-- make sure you assign unique number here
#define portNum 12345 //<-- if changed here change in rasbery pi also

String DeviceType = "RelayActuator";
IOTDevice Dev(DeviceType + "_" + IDNo, portNum);

//Code onto WEMOS board
#define RelayPin D5
String parameters[2] = {"ID", "RelayValue"};
String value = "false";

void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);
Dev.setupIOT(SSID, PWD);
pinMode(RelayPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

String Message = Dev.getNewMessage();

if(Message.length() > 0)
{
   if(Message.indexOf("true")>=0)
      {
        value = "true";
        digitalWrite(RelayPin, HIGH);
      }
      else
      {
        value = "false";
         digitalWrite(RelayPin, LOW);
      }
}

 

String values[2] = {Dev.ID, value};
if(Dev.sendMessageOnlyIfDifferent(parameters, values, 2))
{
  //Message Sent
  Serial.print("Relay status: ");
  Serial.println(value);
}

}
