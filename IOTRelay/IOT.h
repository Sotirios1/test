#ifndef IOT_h
#define IOT_h
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>
#define OUTPUTSteps Serial
#define Delimiter ';'
#define SSid "ssid"
#define CODE "code"
#define IP "IP"
#define PORT "Port"
#define IDi "ID"
#define Pwd "Pwd"

#ifndef MAX_NO_SENDER
#define MAX_NO_SENDER 10
#endif // !MAX_NO_SENDER

//#define DEBUG

#define DEBUG_PRINT(x)   OUTPUTSteps.print(x)
#define DEBUG_PRINTln(x)  OUTPUTSteps.println(x)

#define MaxMemoryValues 10
#ifndef DEBUG
#else
//#define DEBUG_PRINT(x)
//#define DEBUG_PRINTln(x) 
#endif

/*
* This class will convert WeMos to an Internet of Things Device
* Packet transfer is done using UDP
*/
class IOTDevice
{
protected:
	int ccount;
	String oldvalues[MaxMemoryValues];
public:
	String ID;
	unsigned int localUdpPort;
	WiFiUDP Udp;

	String senderIP[MAX_NO_SENDER];
	unsigned int senderUdpPort[MAX_NO_SENDER];
	String latestSenderIP;
	unsigned int latestSenderPort;
	int maxSenderCountRegistered;
public:
	/* Select the unique IOT ID and the port number that it will listen on */
	IOTDevice(String IOTID, unsigned int localUdpPort);
	
	
	bool startConfiguring(String ssid, String pwd );
	/* A blocking call till IOT is configured with atleast one sender*/
	bool setupIOT(String ssid, String pwd, bool isBeacon = true);



	bool connectWifi(String ssid, String pwd);
	bool tryConnectWifi(String ssid, String pwd, int noTrys=30);
	bool bindToNewUDPPort(unsigned int portNumber);
	void disconnectWifi();

	void BroadCastMyPresence();
	bool isMessageReceivedForNewWifiConnection(String message, bool act = true);
	bool isMessageReceivedForNewSender(String message, bool act = true);
	bool isIOTConfigureChangeRequest(String message, bool act = true);

	bool AddSenderToService(String MessageReceived);
	bool AddSenderToService(String IPaddress, unsigned int PortNo);
	/* Add sender of the last received message to the list*/
	void lockSendersDetails();

	String getValue(String message, String key);
	//bool changeRouterDevice(String new_ssid, String new_pwd);
	String createMessage(String params[], String values[], int n, char delimiter);
	void parseString(String data, int n, String parameter[], String values[], char delimiter);
	
	void sendMessage(String params[], String values[], int n);
	void sendMessage(String sender, unsigned int sender_portNo, String params[], String values[], int n);
	bool gotAnyMessage(String *message, String *ipSender, unsigned int *senderUdpPort);
	String getNewMessage(bool processConfigurationIOTAlso = true);
	String getLatestSenderIP(unsigned int *senderUdpPort);
	bool sendMessageOnlyIfDifferent(String params[], String values[], int n, int thresholdSendAnyways = 5000);


	void printCurrentStatus();



};
#endif
